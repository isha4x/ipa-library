NONE of these are made by me. I just combineded them into a simple place where you can download them. Enjoy!

---------------------------
iPhone and iPad
----------------------

unc0ver Jailbreak : https://unc0ver.dev/downloads/5.3.1/72004596b31ba3eae886ac6cc781725879d3b7a4/unc0ver_5.3.1.ipa

unc0ver Multipath : https://ipafiles.ml/unc0vermulti.ipa

Chimera Jailbreak : https://chimera.sh/downloads/ios/1.4.0-12.0-12.4.ipa 

Rootless JB : https://ipafiles.ml/rootlessjb.ipa

Electra : https://github.com/coolstar/electra-ipas/raw/master/Electra1141-1.3.2.ipa

Pangu : https://ipafiles.ml/pangu.ipa 

Pheonix : https://ipafiles.ml/phoenix.ipa

Saigon : https://ipafiles.ml/saigon.ipa

h3lix : https://iosninja.wetransfer.com/downloads/17726b2a72f2317e94e61fc79c87375420191120041032/4900db

Oddessey : https://iosninja.wetransfer.com/downloads/cfc44c7138adf0c2ce890e9d405f47b920200829211600/8e5761

------------------------
TV
------------------------

unc0ver : https://iosninja.wetransfer.com/downloads/7bbc7eb988923a5d2b3d929fae83f3cf20200723191124/1f5d46

Electra : https://raw.githubusercontent.com/coolstar/electra-ipas/master/ElectraTV-1.3.2.ipa

Chimera Jailbreak TV : https://chimera.sh/downloads/tv/1.3.9.ipa






----------------------
Tools
----------------------
Sileo (Cydia Alternative) https://iosninja.wetransfer.com/downloads/0c61fd6bf13177e831b3c76ff95d9b1020190728153222/2be5d9

Anti Revoke - iOS 12 : https://iosninja.io/ipa-library/download-lazarusjailed-app-ipa-iphone
